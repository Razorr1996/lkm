#!/usr/bin/python

# -*- coding: utf-8 -*-
import sys
import os
from pprint import pprint
from time import sleep
import json
import socket


def contDir(dirArg):
    res = []
    for i in dirArg:
        if os.path.isdir(i):
            for j in os.walk(i):
                res.append(j)
    return res


def compareSnap(fileDictOld, fileDict):
    fileSet = set(fileDict.keys())
    fileSetOld = set(fileDictOld.keys())
    added = 'Added'
    modded = 'Modded'
    deleted = 'Deleted'
    res = {added: [], deleted: [], modded: []}
    for i in fileSetOld & fileSet:
        if fileDict[i] > fileDictOld[i]:
            res[modded].append((i, os.path.getsize(i)))
    for i in fileSet - fileSetOld:
        res[added].append((i, os.path.getsize(i)))
    for i in fileSetOld - fileSet:
        res[deleted].append((i, 0))
    return res


def viewDir(dirArg):
    res = {}
    for i in contDir(dirArg):
        for j in i[2]:
            fName = os.path.join(i[0], j)
            res[fName] = os.path.getmtime(fName)
    return res


def sendStr(ip, port, bArr):
    conn = socket.socket()
    conn.connect((ip, port))
    conn.send(bArr)
    conn.close()


if sys.argv.__len__() < 4:
    print('Usage: {0} IP PORT DIR1 [DIR2 ...]'.format(sys.argv[0]))
    sys.exit(1)
else:
    ip = sys.argv[1]
    port = int(sys.argv[2])
    dirs = sys.argv[3:]

pprint(contDir(dirs))

print()
fileDictOld = viewDir(dirs)
fileDict = {}

while True:
    fileDict = viewDir(dirs)
    fChange = compareSnap(fileDictOld, fileDict)
    # pprint(fChange)
    jstr = json.dumps(fChange, ensure_ascii=False)
    sendStr(ip, port, jstr.encode('utf8'))
    pprint(jstr)
    print()
    fileDictOld = fileDict
    sleep(2)
