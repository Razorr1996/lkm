#!/usr/bin/python

# -*- coding: utf-8 -*-
import asyncore
import socket
import sys
import json
from datetime import datetime


class MainServerSocket(asyncore.dispatcher):
    def __init__(self, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(('', port))
        self.listen(50)

    def handle_accept(self):
        newSocket, address = self.accept()
        # print("Connected from {0}".format(address))
        SecondaryServerSocket(newSocket)


class SecondaryServerSocket(asyncore.dispatcher_with_send):
    def handle_read(self):
        receivedData = self.recv(8192)
        if receivedData:
            self.send(receivedData)
            sss = receivedData.decode("utf8")
            jOut(json.loads(sss))
        self.close()


# def handle_close(self):
#     print(end='')


def jOut(s):
    timeStr = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")
    same = False
    for i in s:
        if s[i]:
            same = True
            print('{0}\t{1}:'.format(i, timeStr))
            for j in s[i]:
                print('\tName:{0}\tSize:{1}'.format(j[0], j[1]))
    if same:
        print()


if sys.argv.__len__() < 2:
    print('Usage: {0} PORT'.format(sys.argv[0]))
    sys.exit(1)
else:
    pArg = int(sys.argv[1])

MainServerSocket(pArg)
asyncore.loop()
