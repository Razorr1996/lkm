#!/bin/bash

rrr=".tmp$RANDOM"
if ! [ -d "$1" ]
	then
	echo "USAGE: $0 DIR"
	exit 1
fi
echo "HOME=$PWD
20 4 * * * ./script.sh $1" > "$rrr"
crontab "$rrr"
rm "$rrr"
