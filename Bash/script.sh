#!/bin/bash
echo "For periodic job use ./cron.sh DIR"
if ! [ -d "$1" ]
	then
	echo "USAGE: $0 DIR"
	exit 1
fi
log="/var/log/sync.log"
dst=~/data/to_send/
OLDIFS="$IFS"
IFS=$'\n'
array=( `find "$1" -type f -mtime -7 -daystart -print0 | xargs -0 ls -1t | head -2` )
IFS="$OLDIFS"
for rec in "${array[@]}"; do
	# echo ["$rec"]
	fName=`echo "$rec" | sed 's|^.*/||g'`
	fTarName="$fName".tgz
	fPath=`echo "$rec" | sed 's|[^/]*$||g'`
	echo "[Time:`date`] [Path:$fPath] [Name:$fName] [Size:`stat "$rec" -c "%s"`]" >> "$log"
	cp "$rec" "$fName"
	tar -czvf "$fTarName" "$rec"
	rm "$fName"
	if [[ "$?" == 0 ]]; then
		cp "$fTarName" "$dst"
		if [[ "$?" == 0 ]]; then
			rm "$fTarName"
			rm "$rec"
		fi
	fi
done
# 20 4 * * * /home/razorr/Dropbox/IDEA/Py/script.sh /home/razorr/Dropbox/Job/tmp
