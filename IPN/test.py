n = int(input())
d = {}
for i in range(n - 1):
    [chld, prnt] = input().split()
    # print('P={}; C={}'.format(prnt, chld))
    l = []
    if prnt in d:
        l = d[prnt][:]
    else:
        d[prnt] = l[:]
    l.append(prnt)
    d[chld] = l[:]
    for j in d:
        if d[j]:
            if d[j][0] == chld:
                d[j] = l + d[j]
# print(d)

m = int(input())
for i in range(m):
    [p1, p2] = input().split()
    a1 = d[p1] + [p1]
    a2 = d[p2] + [p2]
    r = a1[0]
    for j in range(min(len(a1), len(a2))):
        if a1[j] == a2[j]:
            r = a1[j]
        else:
            break
    print(r)
